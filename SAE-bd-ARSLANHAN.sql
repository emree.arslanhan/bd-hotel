DROP TABLE RESERVER CASCADE CONSTRAINT;

DROP TABLE COMMANDER;

DROP TABLE SERVICES;

DROP TABLE CLIENT CASCADE CONSTRAINT ;

DROP TABLE CHAMBRE;

DROP TABLE PERSONNEL;

DROP TABLE DEPARTEMENT;

DROP TABLE PERSONNE;

DROP TABLE HOTEL;

CREATE TABLE HOTEL(
    IdHotel NUMBER(15),
    NbChambre NUMBER(15),
    NiveauxDeQualite NUMBER(15),
    VilleH VARCHAR2(20),
    RueH VARCHAR2(20),
    GPS VARCHAR2(20),
    NomH VARCHAR2(20),
    CONSTRAINT pkHotel PRIMARY KEY (IdHotel)
);

CREATE TABLE PERSONNE(
    IDPersonne NUMBER(15),
    Nom VARCHAR2(20),
    Adresse VARCHAR2(20),
    DnD DATE,
    CONSTRAINT pkIDper PRIMARY KEY (IDPersonne)
);

CREATE TABLE DEPARTEMENT(
    NomDepartement VARCHAR2(20),
    IdHotel NUMBER(15) NOT NULL,
    NumTelephonique NUMBER(15),
    NomResponsable VARCHAR2(20),
    CONSTRAINT pkNomDep PRIMARY KEY (IdHotel,NomDepartement),
    CONSTRAINT fkhotel FOREIGN KEY (IdHotel) REFERENCES HOTEL(IdHotel)
);

CREATE TABLE PERSONNEL(
    IdPersonnel NUMBER(15),
    IDPersonne NUMBER(15) NOT NULL,
    NomDepartement VARCHAR2(20) NOT NULL,
    IdHotel NUMBER(15) NOT NULL,
    FonctionP VARCHAR2(20),
    CONSTRAINT pkIdP PRIMARY KEY (IdPersonnel,IDPersonne),
    CONSTRAINT fkIDP FOREIGN KEY (IDPersonne) REFERENCES PERSONNE(IDPersonne),
    CONSTRAINT fkIDH FOREIGN KEY (IdHotel,NomDepartement) REFERENCES DEPARTEMENT(IdHotel,NomDepartement),
    CONSTRAINT fkidhot FOREIGN KEY (IdHotel) REFERENCES HOTEL(IdHotel)
);

CREATE TABLE CHAMBRE(
    NumChambre NUMBER(15),
    IdHotel NUMBER(15) NOT NULL,
    Superficie NUMBER(15),
    MaxPersonne NUMBER(15),
    CONSTRAINT pkNumC PRIMARY KEY (IdHotel,NumChambre),
    CONSTRAINT fkhot FOREIGN KEY (IdHotel) REFERENCES HOTEL(IdHotel)
);

CREATE TABLE CLIENT(
    IDPersonne NUMBER(15) NOT NULL,
    IdClient NUMBER(15),
    nationalite VARCHAR2(20),
    CONSTRAINT pkIdCl PRIMARY KEY (IDPersonne,IdClient),
    CONSTRAINT fkIdPe FOREIGN KEY (IDPersonne) REFERENCES PERSONNE(IDPersonne)
);

CREATE TABLE SERVICES(
    IdService NUMBER(15),
    NomS VARCHAR2(20),
    DescriptionS VARCHAR2(100),
    PrixS NUMBER(15),
    CONSTRAINT pkIdS PRIMARY KEY (IdService)
);

CREATE TABLE COMMANDER(
    IdService NUMBER(15) NOT NULL,
    Heure NUMBER(15),
    NumChambre NUMBER(15) NOT NULL,
    IdHotel NUMBER(15) NOT NULL,
    Quantite NUMBER(15),
    Bool NUMBER(15),
    CONSTRAINT pkhe PRIMARY KEY (Heure,IdService,NumChambre,IdHotel),
    CONSTRAINT fkhote FOREIGN KEY (IdHotel) REFERENCES HOTEL (IdHotel),
    CONSTRAINT fNumc FOREIGN KEY (IdHotel,NumChambre) REFERENCES CHAMBRE (IdHotel,NumChambre),
    CONSTRAINT fkIdse FOREIGN KEY (IdService) REFERENCES SERVICES (IdService)  
);

CREATE TABLE RESERVER( 
    IdHotel NUMBER(15) NOT NULL,
    NumChambre NUMBER(15) NOT NULL,
    IdClient NUMBER(15) NOT NULL,
    IDPersonne NUMBER(15) NOT NULL,
    IdService NUMBER(15) NOT NULL,
    datee_r DATE,
    datee_de_fin DATE,
    Prix NUMBER(15),
    NombrePersonne NUMBER(15),
    CONSTRAINT pkdat PRIMARY KEY (IdClient,NumChambre,IdHotel,IDPersonne,datee_r),
    CONSTRAINT fkho FOREIGN KEY (IdHotel) REFERENCES HOTEL (IdHotel),
    CONSTRAINT fNumch FOREIGN KEY (IdHotel,NumChambre) REFERENCES CHAMBRE (IdHotel,NumChambre),
    CONSTRAINT fkIdser FOREIGN KEY (IdService) REFERENCES SERVICES (IdService),
    CONSTRAINT fkIdPee FOREIGN KEY (IDPersonne) REFERENCES PERSONNE(IDPersonne),
    CONSTRAINT fkIDc FOREIGN KEY (IDPersonne,IdClient) REFERENCES CLIENT(IDPersonne,IdClient)  
);

INSERT INTO HOTEL( IdHotel, NbChambre, NiveauxDeQualite, VilleH, RueH, GPS, NomH)
VALUES (10045,220,5,'Orléans','10rueRabot','61124','Palace');
INSERT INTO HOTEL( IdHotel, NbChambre, NiveauxDeQualite, VilleH, RueH, GPS ,NomH)
VALUES (11045,250,5,'Blois','10rueBouc','65524','Resort');
INSERT INTO HOTEL( IdHotel, NbChambre, NiveauxDeQualite, VilleH, RueH, GPS ,NomH)
VALUES (10445,237,5,'Marseille','10rueKennedy','61584','Sirena');

INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (0123,'Emre','10rueMonge', TO_DATE('11032003', 'DDMMYYYY'));
INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (0124,'Patrick','15ruePat', TO_DATE('05031993', 'DDMMYYYY'));
INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (0125,'Charlie','24rueChap', TO_DATE('22061996', 'DDMMYYYY'));
INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (201,'Dubo','12rueParapluie',TO_DATE('29091990', 'DDMMYYYY'));
INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (202,'Castle','12rueNuketown',TO_DATE('19041985', 'DDMMYYYY'));
INSERT INTO PERSONNE( IDPersonne,Nom,Adresse,DnD)
VALUES (203,'Holmes','12rueLondon',TO_DATE('03091979', 'DDMMYYYY'));


INSERT INTO DEPARTEMENT( IdHotel, NomDepartement, NumTelephonique ,NomResponsable)
VALUES (10045,'BAR',0238988220,'Michel');
INSERT INTO DEPARTEMENT( IdHotel, NomDepartement, NumTelephonique, NomResponsable)
VALUES (11045,'CUISINE',0278588220,'Franklin');
INSERT INTO DEPARTEMENT( IdHotel, NomDepartement, NumTelephonique, NomResponsable)
VALUES (10445,'RECEPTION',0234938220,'Trevor');

INSERT INTO PERSONNEL( IdPersonnel, IDPersonne, NomDepartement, IdHotel, FonctionP)
VALUES (0012,0123,'BAR',10045,'Barman');
INSERT INTO PERSONNEL( IdPersonnel, IDPersonne, NomDepartement, IdHotel, FonctionP)
VALUES (0013,0124,'CUISINE',11045,'Cuisinier');
INSERT INTO PERSONNEL( IdPersonnel, IDPersonne, NomDepartement, IdHotel, FonctionP)
VALUES (0014,0125,'RECEPTION',10445,'Receptionniste');

INSERT INTO CHAMBRE( IdHotel, NumChambre, Superficie, MaxPersonne)
VALUES (10045,1,31,3);
INSERT INTO CHAMBRE( IdHotel, NumChambre, Superficie, MaxPersonne)
VALUES (11045,5,36,4);
INSERT INTO CHAMBRE( IdHotel, NumChambre, Superficie, MaxPersonne)
VALUES (10445,13,43,5);

INSERT INTO CLIENT( IdClient, IDPersonne, nationalite)
VALUES (2001,201,'Français');
INSERT INTO CLIENT( IdClient, IDPersonne, nationalite)
VALUES (2002,202,'Américain');
INSERT INTO CLIENT( IdClient, IDPersonne, nationalite)
VALUES (2003,203,'Anglais');

INSERT INTO SERVICES( IdService, NomS, DescriptionS, PrixS)
VALUES (3000,'Vin','Un delicieux vin rouge ou blanc',50);
INSERT INTO SERVICES( IdService, NomS, DescriptionS, PrixS)
VALUES (3010,'Burger','Un burger fait maison pour vous',10);
INSERT INTO SERVICES( IdService, NomS, DescriptionS, PrixS)
VALUES (3020,'Glace','Une glace à la vanille/chocolat/fraise',5);

INSERT INTO COMMANDER( IdHotel, NumChambre, IdService, Heure, quantite, Bool)
VALUES (10045,1,3000,11,2,1);
INSERT INTO COMMANDER(IdHotel, NumChambre, IdService, Heure, quantite, Bool)
VALUES (11045,5,3020,16,3,0);
INSERT INTO COMMANDER( IdHotel, NumChambre, IdService, Heure, quantite, Bool)
VALUES (10445,13,3010,21,4,1);

INSERT INTO RESERVER( IdHotel, NumChambre, IDPersonne, IdClient, IdService, datee_r, datee_de_fin, Prix , NombrePersonne)
VALUES (10045,1,201,2001,3000,TO_DATE('10092021', 'DDMMYYYY'),TO_DATE('20092021', 'DDMMYYYY'),1600,2);
INSERT INTO RESERVER( IdHotel, NumChambre, IDPersonne, IdClient, IdService, datee_r, datee_de_fin, Prix , NombrePersonne)
VALUES (11045,5,202,2002,3010,TO_DATE('10092021', 'DDMMYYYY'),TO_DATE('17092021', 'DDMMYYYY'),500,1);
INSERT INTO RESERVER( IdHotel, NumChambre, IDPersonne, IdClient, IdService, datee_r, datee_de_fin, Prix , NombrePersonne)
VALUES (10445,13,203,2003,3020,TO_DATE('10092021', 'DDMMYYYY'),TO_DATE('16092021', 'DDMMYYYY'),1800,3);

--les valeurs a partir de la sont faux , il permette de tester les contraintes

--INSERT INTO CHAMBRE( IdHotel, NumChambre, Superficie, MaxPersonne)
--VALUES (10045,1,37,4); -- cette ligne n'est pas possible

--INSERT INTO CLIENT( IdClient, IDPersonne, nationalite) 
--VALUES(2001,201,'Hollandais'); --cette ligne n'est pas possible
